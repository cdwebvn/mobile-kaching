﻿(function () {
	'use strict';

	var module = angular.module('app');
	
	
})();

(function (angular, _) {
	'use strict';

	var services = angular.module('services');

	var currentUser = null;

	services.factory('AuthService', function ($http, $cookies, $rootScope, $timeout, UserService, appConfig, $q, Restangular) {
		var baseApiUrl = appConfig.authApiUrl;
		var COOKIE_NAME = appConfig.cookieName;
		var service = {
		};

		return service;
	});

	services.factory('bearerTokenInterceptor', ['$log', '$q', '$location', function ($log, $q, $location) {
		//$log.debug('$log is here to show you that this is a regular factory with injection');

		var bearerTokenInterceptor = {
			'request': function (config) {
				//console.info(config);
				if (config.method === "POST" && !config.noIdentity) {
					if (currentUser) {
						config.data = _.assign(config.data, { email: currentUser.email, password: currentUser.password });
					}
				}

				return config;
			},
			'responseError': function (response) {
				if (response.status === 401) {
					$location.path('/login');
				}

				return $q.reject(response);
			}
		};

		return bearerTokenInterceptor;
	}]);


})(angular, _);

