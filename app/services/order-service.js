(function (angular, _) {
	'use strict';

	var services = angular.module('services');

	services.factory('OrderService', ['$log', '$http', '$q', 'Restangular', 'BaseService', 
		function ($log, $http, $q, Restangular, BaseService) {

		function getOrders(params) {
			var deferred = $q.defer();
			var errorMessage = "";

			Restangular.all("order/list").post(params).then(function (result) {
				if (result.status_code == 200) {
					deferred.resolve(result.orders);
				}
				else {
					deferred.reject(errorMessage);
				}
			}).catch(function (res) {
				//errorMessage = this.getErrorMessages(res, errorMessage);
				deferred.reject(errorMessage);
			});
			

			return deferred.promise;
		}

		function OrderService() {
			BaseService.call(this);
		}

		OrderService.prototype = _.create(BaseService.prototype, {
			'constructor': BaseService,

			getOrders: getOrders
		});

		var service = new OrderService;
		return service;
	}]);



})(angular, _);


