(function (angular, _, undefined) {
	'use strict';

	var app = angular.module('app');

	app.config(['$stateProvider', function config($stateProvider) {

		//#region Login
		$stateProvider.state('login', {
			abstract: true,
			url: "/login",
			templateUrl: "views/layout/home-layout.html",
			controller: 'AuthController',
			authenticate: false
		});

		$stateProvider.state('login.signIn', {
			url: '',
			templateUrl: 'views/auth/sign-in.html',
			controller: 'SignInController',
			authenticate: false
		});
		

		$stateProvider.state('home', {
			abstract: true,
			url: "/home",
			templateUrl: "views/layout/login-layout.html",
			controller: 'AuthController',
			authenticate: false
		});

		$stateProvider.state('home.landingPage', {
			url: '',
			templateUrl: 'views/home/home.html',
			controller: 'HomeController',
			authenticate: false
		});

		

	}]);


})(angular, _);