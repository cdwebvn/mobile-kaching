(function (angular, _) {
	'use strict';

	var directives = angular.module('directives');

	directives.directive('productImage', [function () {
		return {
			restrict: "C",
			scope: {
				product: "="
			},
			link: function (scope, element, attr) {
				var imageFile;
				element.on("change", function (event) {
					var reader = new FileReader();
					var imageFile = event.target.files[0];
					reader.onload = function (e) {
						scope.$apply(function () {
							scope.product['image_url'] = e.target.result;
							scope.product.file_name = imageFile.name;
						})
					};
					reader.readAsDataURL(imageFile);
					event.target.value = "";
				});
			}
		};
	}]);

})(angular, _);