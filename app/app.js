(function (angular) {
	"use strict";

	try {
		angular.module('templates');
	} catch (e) {
		angular.module('templates', []);
	}

	var app = angular.module('app', [
		'constants',
		'templates',
		'ui.router',
		'ngCookies',
		'ui.router',
		'ui.bootstrap',
		'ngSanitize',
		'services',
		'filters',
		'directives',
		'models',
		'controllers',
		'restangular',
		'vcRecaptcha',
		'ngHelperDynamicTemplateLoader',
		'daterangepicker',
		'ui.map']);

	/* Setup global settings */
	app.factory('settings', ['$rootScope', function ($rootScope) {
		// supported languages
		var settings = {
			layout: {
				isHidePageHeader : false,
				bodyLayout: ''
			},
		};

		$rootScope.settings = settings;

		return settings;
	}]);

	app.config(['$urlRouterProvider', '$stateProvider', '$locationProvider', '$httpProvider', '$dynamicTemplateLoaderProvider', 'appConfig',
	function config($urlRouterProvider, $stateProvider, $locationProvider, $httpProvider, $dynamicTemplateLoaderProvider, appConfig) {
		$urlRouterProvider.otherwise("/");
		//$urlRouterProvider.otherwise("/dashboard");

		$locationProvider.html5Mode({
			enabled: false,
			requireBase: false
		});

		// Register the HTTP interceptors
		if (appConfig.debug) {
			$dynamicTemplateLoaderProvider.registerInterceptors();
		}

		$httpProvider.interceptors.push('bearerTokenInterceptor');

		// highchartsNGProvider.lazyLoad([highchartsNGProvider.HIGHCHART/HIGHSTOCK, "maps/modules/map.js", "mapdata/custom/world.js"]);// you may add any additional modules and they will be loaded in the same sequence

	    

	}]);

	app.run(['$rootScope', '$state', '$stateParams', 'settings', '$http', '$window', '$timeout', 'appConfig', 'Restangular', 'AuthService',
	function run($rootScope, $state, $stateParams, settings, $http, $window, $timeout, appConfig, Restangular, AuthService) {
		/* Init global settings and run the app */
		 $rootScope.$state = $state; // state to be accessed from view
		 $rootScope.$settings = settings; // state to be accessed from view
		 $rootScope.$stateParams = $stateParams;

	}]);


})(angular);
