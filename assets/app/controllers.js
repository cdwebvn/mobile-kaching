(function (angular) {
	"use strict";

	var controllers = angular.module('controllers', [
		'services',
		'filters',
		'models',
		"ui.bootstrap",
		'ngStorage',
		'angularFileUpload',
		'vcRecaptcha'
	]);

})(angular);
