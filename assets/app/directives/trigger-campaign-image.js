(function (angular, _) {
	'use strict';

	var directives = angular.module('directives');

	directives.directive("triggerCampaignImage", ['$timeout', function ($timeout) {
		return {
			restrict: "C",
			scope: {
				trigger: "="
			},
			link: function (scope, element) {
				var imageFile;
				element.on("change", function (event) {
					var reader = new FileReader();
					var imageFile = event.target.files[0];
					reader.onload = function (e) {
						scope.$apply(function () {
							scope.trigger['image_url'] = e.target.result;
							scope.trigger.file_name = imageFile.name;
						})
					};
					reader.readAsDataURL(imageFile);
					event.target.value = "";
				});
			}
		}
	}])

})(angular, _);