(function (angular, _) {
	'use strict';

	var directives = angular.module('directives');

	directives.directive('pageFooter', [function () {
		return {
			replace: true,
			templateUrl: 'views/common/page-header.html',
			link: function (scope, el, attr) {

			}
		};
	}]);

})(angular, _);


