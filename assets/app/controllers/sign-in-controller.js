(function (angular, _) {
	'use strict';

	var controllers = angular.module('controllers');

	controllers.controller('SignInController', ['$scope', '$rootScope', '$state', 'appConfig', 'AuthService', 'PopupMessageService', function ($scope, $rootScope, $state, appConfig, AuthService, PopupMessageService) {

		$scope.goSignUp = function(){
			$state.go('login.signUp');
		}
	}]);


})(angular, _);


