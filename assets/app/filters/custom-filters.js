(function (angular,_) {
	'use strict';

	var filters = angular.module('filters');

	filters.filter('fileType', function () {
		return function (ext) {
			ext = _.trim(ext);
			return !ext ? 'File' : ext.toUpperCase() + ' File';
		};
	});
	
})(angular,_);




