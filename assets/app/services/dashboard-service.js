(function (angular, _) {
	'use strict';

	var services = angular.module('services');

	services.factory('DashboardService', ['$log', '$http', '$q', 'Restangular', 'BaseService', function ($log, $http, $q, Restangular, BaseService) {
		var dashboards = Restangular.all('query/count_by_date');

		function getDashboard(options) {
			var deferred = $q.defer();
			var errorMessage = "";

			dashboards.post(options).then(function (result) {
				if (result.status_code == 200) {
					deferred.resolve(result.results);
				}
				else {
					deferred.reject(errorMessage);
				}
			}).catch(function (res) {
				deferred.reject(errorMessage);
			});
			return deferred.promise;
		}

		function DashboardService() {
			BaseService.call(this);
		}

		DashboardService.prototype = _.create(BaseService.prototype, {
			'constructor': BaseService,
			getDashboard: getDashboard,
		});

		var service = new DashboardService;
		return service;
	}]);



})(angular, _);


